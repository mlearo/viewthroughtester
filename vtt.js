

(function(w){
	const u = 'https://databridge.tdbtrk.com/databridge/';

	function getData(url) {
		var xhr = new XMLHttpRequest();
	    xhr.withCredentials = true;
	    
	    xhr.onreadystatechange = function() {
	        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
	        	var response = JSON.parse(xhr.response);
	        	tealiumTools.send({'result':{'result': response}})
	        } else {
	        	tealiumTools.send({'status': {'status': 'Pixel failed, check your format and try again.'}})
	        }
	    }
	    xhr.open('GET', u + url + '/')
	    xhr.send();
	}
	
	function firePixel(url) {
		var xhr = new XMLHttpRequest();
	    xhr.withCredentials = true;
	    xhr.onreadystatechange = function() {
	        if (xhr.readyState === XMLHttpRequest.DONE) {
	            if (xhr.status === 200) {
	            	tealiumTools.send({'status': {'status': "Pixel fired successfully, ... retrieving data"}})
	            	let id = xhr.responseURL.split('/')[4]
	            	getData(id)
	            } else {
	            	tealiumTools.send({'status': {'status': 'Pixel failed, check your format and try again.'}})
	            }
	        } else {
	        	tealiumTools.send({'status': {'status': 'Pixel failed, check your format and try again.'}})
	        }
	    }
	    xhr.open('GET', url)
	    xhr.send();
	}

	w["__firePixel"] = firePixel;
	w["__getData"] = getData;

}
)(window || {})


tealiumTools.send({"data": false})